#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "simplegrade.h"
#include "sim.h"

void teste1();
void teste2();
void teste3();
void teste4();
void teste5();
void mac();

int main() {

	teste1();
	teste2();
	teste3();
	teste4();
	teste5();
	//mac();

}

void teste1() {

	struct stats *res = malloc(sizeof(struct stats));
	
	res = sim(32, 8192, 8, "my_trace.txt", NULL);

	printf("Teste 1: realiza o Teste utilizando o arquivo my_trace.txt\n");
	if (res->hits_l1 == 8) printf("hits_l1 = %lu: PASSED!\n", res->hits_l1);
	else printf("hits_l1 = %lu: ERROR! O valor correto seria 8, verifique novamente a função verificaCache\n", res->hits_l1);

	if (res->misses_l1 == 2) printf("misses_l1 = %lu: PASSED!\n", res->misses_l1);
	else printf("misses_l1 = %lu: ERROR! O valor correto seria 2, verifique novamente a função verificaCache\n", res->misses_l1);

	if (res->hits_tlb == 8) printf("hits_tlb = %lu: PASSED!\n", res->hits_tlb);
	else printf("hits_tlb = %lu: ERROR! O valor correto seria 8, verifique novamente a função verificaTLB\n", res->hits_tlb);

	if (res->misses_tlb == 2) printf("misses_tlb = %lu: PASSED!\n", res->misses_tlb);
	else printf("misses_tlb = %lu: ERROR! O valor correto seria 2, verifique novamente a função verificaTLB\n", res->misses_tlb);

	if (res->cycles == 230) printf("cycles = %lu: PASSED!\n\n", res->cycles);
	else printf("cycles = %lu: ERROR! O valor correto seria 230, verifique novamente o cálculo de ciclos\n\n", res->cycles);
	res = NULL;
	free(res);
}	

void teste2() {

	struct stats *res = malloc(sizeof(struct stats));
	
	res = sim(32, 8192, 4, "testa_cache.txt", NULL);

	printf("Teste 2: realiza o teste utilizando o arquivo testea_cache.txt\n");
	if (res->hits_l1 == 5) printf("hits_l1 = %lu: PASSED!\n", res->hits_l1);
	else printf("hits_l1 = %lu: ERROR! O valor correto seria 5, verifique novamente a função verificaCache\n", res->hits_l1);

	if (res->misses_l1 == 6) printf("misses_l1 = %lu: PASSED!\n", res->misses_l1);
	else printf("misses_l1 = %lu: ERROR! O valor correto seria 6, verifique novamente a função verificaCache\n", res->misses_l1);

	if (res->hits_tlb == 5) printf("hits_tlb = %lu: PASSED!\n", res->hits_tlb);
	else printf("hits_tlb = %lu: ERROR! O valor correto seria 5, verifique novamente a função verificaTLB\n", res->hits_tlb);

	if (res->misses_tlb == 6) printf("misses_tlb = %lu: PASSED!\n", res->misses_tlb);
	else printf("misses_tlb = %lu: ERROR! O valor correto seria 6, verifique novamente a função verificaTLB\n", res->misses_tlb);

	if (res->cycles == 622) printf("cycles = %lu: PASSED!\n\n", res->cycles);
	else printf("cycles = %lu: ERROR! O valor correto seria 622, verifique novamente o cálculo de ciclos\n\n", res->cycles);
	res = NULL;
	free(res);
}

void teste3() {

	struct stats *res = malloc(sizeof(struct stats));
	
	res = sim(64, 8192, 8, "MAC_10.txt", NULL);

	printf("Teste 3: realiza o teste utilizando o arquivo MAC_10.txt\n");
	if (res->hits_l1 == 28) printf("hits_l1 = %lu: PASSED!\n", res->hits_l1);
	else printf("hits_l1 = %lu: ERROR! O valor correto seria 28, verifique novamente a função verificaCache\n", res->hits_l1);

	if (res->misses_l1 == 12) printf("misses_l1 = %lu: PASSED!\n", res->misses_l1);
	else printf("misses_l1 = %lu: ERROR! O valor correto seria 12, verifique novamente a função verificaCache\n", res->misses_l1);

	if (res->hits_tlb == 33) printf("hits_tlb = %lu: PASSED!\n", res->hits_tlb);
	else printf("hits_tlb = %lu: ERROR! O valor correto seria 33, verifique novamente a função verificaTLB\n", res->hits_tlb);

	if (res->misses_tlb == 7) printf("misses_tlb = %lu: PASSED!\n", res->misses_tlb);
	else printf("misses_tlb = %lu: ERROR! O valor correto seria 7, verifique novamente a função verificaTLB\n", res->misses_tlb);

	if (res->cycles == 1070) printf("cycles = %lu: PASSED!\n\n", res->cycles);
	else printf("cycles = %lu: ERROR! O valor correto seria 1070, verifique novamente o cálculo de ciclos\n\n", res->cycles);
	res = NULL;
	free(res);
}		
	
void teste4() {

	struct stats *res = malloc(sizeof(struct stats));
	
	res = sim(64, 8192, 4, "MAC_512.txt", NULL);

	printf("Teste 4: realiza o teste utilizando o arquivo MAC_512.txt\n");
	if (res->hits_l1 == 1440) printf("hits_l1 = %lu: PASSED!\n", res->hits_l1);
	else printf("hits_l1 = %lu: ERROR! O valor correto seria 1440, verifique novamente a função verificaCache\n", res->hits_l1);

	if (res->misses_l1 == 608) printf("misses_l1 = %lu: PASSED!\n", res->misses_l1);
	else printf("misses_l1 = %lu: ERROR! O valor correto seria 608, verifique novamente a função verificaCache\n", res->misses_l1);

	if (res->hits_tlb == 1530) printf("hits_tlb = %lu: PASSED!\n", res->hits_tlb);
	else printf("hits_tlb = %lu: ERROR! O valor correto seria 1530, verifique novamente a função verificaTLB\n", res->hits_tlb);

	if (res->misses_tlb == 518) printf("misses_tlb = %lu: PASSED!\n", res->misses_tlb);
	else printf("misses_tlb = %lu: ERROR! O valor correto seria 518, verifique novamente a função verificaTLB\n", res->misses_tlb);

	if (res->cycles == 60396) printf("cycles = %lu: PASSED!\n\n", res->cycles);
	else printf("cycles = %lu: ERROR! O valor correto seria 60396, verifique novamente o cálculo de ciclos\n\n", res->cycles);
	res = NULL;
	free(res);
}	

void teste5() {

	struct stats *res = malloc(sizeof(struct stats));
	
	res = sim(64, 16384, 8, "MAC_1024.txt", NULL);

	printf("Teste 5: realiza o teste utilizando o arquivo MAC_1024.txt\n");
	if (res->hits_l1 == 2880) printf("hits_l1 = %lu: PASSED!\n", res->hits_l1);
	else printf("hits_l1 = %lu: ERROR! O valor correto seria 2880, verifique novamente a função verificaCache\n", res->hits_l1);

	if (res->misses_l1 == 1216) printf("misses_l1 = %lu: PASSED!\n", res->misses_l1);
	else printf("misses_l1 = %lu: ERROR! O valor correto seria 1216, verifique novamente a função verificaCache\n", res->misses_l1);

	if (res->hits_tlb == 3060) printf("hits_tlb = %lu: PASSED!\n", res->hits_tlb);
	else printf("hits_tlb = %lu: ERROR! O valor correto seria 3060, verifique novamente a função verificaTLB\n", res->hits_tlb);

	if (res->misses_tlb == 1036) printf("misses_tlb = %lu: PASSED!\n", res->misses_tlb);
	else printf("misses_tlb = %lu: ERROR! O valor correto seria 1036, verifique novamente a função verificaTLB\n", res->misses_tlb);

	if (res->cycles == 128984) printf("cycles = %lu: PASSED!\n\n", res->cycles);
	else printf("cycles = %lu: ERROR! O valor correto seria 128984, verifique novamente o cálculo de ciclos\n\n", res->cycles);
	res = NULL;
	free(res);
}	

void mac() {

	int i, j = 0, tam = 10, k;
	char mac[13] = "MAC_", tamanho[5], nome[13] = "MAC_";
	int *a, *b, *c;
	FILE *arq;
	
	a = malloc(10*sizeof(int));
	b = malloc(10*sizeof(int));
	c = malloc(10*sizeof(int));
		
	for (k = 0; k < 3; k++) {
		
			
		j = 0;
		if (k == 1) { 

			tam = 512;
			a = realloc(a, tam*sizeof(int));
			b = realloc(b, tam*sizeof(int));
			c = realloc(c, tam*sizeof(int));
		}
		
		if (k == 2) {

			tam = 1024;
			a = realloc(a, tam*sizeof(int));
			b = realloc(b, tam*sizeof(int));
			c = realloc(c, tam*sizeof(int));
		}
		
		sprintf(tamanho, "%d.txt", tam);
		strncat(mac, tamanho, strlen(tamanho));
		
		arq = fopen(mac, "ab+");
		
		for (i = 0; i < tam; i++) {

			a[i] = j;
			b[i] = j + tam*4;
			c[i] = j + tam*8;

			fprintf(arq, "R %.8X\nR %.8X\nR %.8X\n", a[i], b[i], c[i]);

			a[i] = a[i] + b[i]*c[i];
	
			fprintf(arq, "W %.8X\n", a[i]);
			
			j += 4;
		}	
		arq = NULL;
		sprintf(mac, "%s", nome);

	}
}	