#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "sim.h"



//============================> VARIÁVEIS GLOBAIS USADAS PELA TLB <============================

int qtd_tlb = 0; //número de posições ocupadas na TLB
int *lru_aux; //vetor que armazena a ordem de acesso à TLB
int lru_aux_index = 0; //índice do vetor lru_aux
int lru_pos = 0; //posição em que se encontra o bloco a ser substituído dentro do vetor lru_aux
int lru = 0; //bloco a ser substituído

int hit = 0, hits = 0, miss = 0, hits_escrita = 0, hits_leitura = 0;

//=============================================================================================

//===========================> VARIÁVEIS GLOBAIS USADAS PELA CACHE <===========================

int qtd_blocos = 0; //quantidade de blocos que a cache possui
int lru_cache = 0; //bloco a ser substituído no 2-associativo
int tam_tag = 0; //número de bits usados na tag

//=============================================================================================

typedef struct cache{
	bool residencia;
	int tag[2];

}CACHE;

typedef struct Tlb{
	bool residencia;
	int endereco;
	
}TLB;

CACHE *iniciaCache(int tamanho_bloco, int tamanho_cache);
TLB *iniciaTLB(int entradas_tlb);
struct stats *iniciaInfo();
bool verificaTLB(TLB *tlb, int endereco_convertido, int entradas_tlb);
int lruTLB();
bool verificaCache(CACHE *cache, int endereco_convertido, int tamanho_bloco, int tamanho_cache, char operacao);

CACHE *iniciaCache(int tamanho_bloco, int tamanho_cache) {
	
	CACHE *cache;
	int i;
	
	if (tamanho_bloco == 32 || tamanho_bloco == 64) {

		if (tamanho_cache == 8192 && tamanho_bloco == 32) {

			cache = malloc(256*sizeof(CACHE));
			qtd_blocos = 256;
			tam_tag = 5 + 8;
			for (i = 0; i < 256; i++) {

				cache[i].residencia = 0;
			}

			return cache;
		}
	
		if (tamanho_cache == 8192 && tamanho_bloco == 64) {

			cache = malloc(128*sizeof(CACHE));
			qtd_blocos = 128;
			tam_tag = 6 + 7;
			for (i = 0; i < 128; i++) {
	
				cache[i].residencia = 0;
			}

			return cache;
		}

		if (tamanho_cache == 16384 && tamanho_bloco == 32) {

			cache = malloc(512*sizeof(CACHE));
			qtd_blocos = 512;
			tam_tag = 5 + 9;
			for (i = 0; i < 512; i++) {
	
				cache[i].residencia = 0;
			}
			
			return cache;
		}
	
		if (tamanho_cache == 16384 && tamanho_bloco == 64) {
	
			cache = malloc(256*sizeof(CACHE));
			qtd_blocos = 256;
			tam_tag = 6 + 8;
			for (i = 0; i < 256; i++) {

				cache[i].residencia = 0;
			}
			return cache;
		}
	}
	
	return NULL;
}

TLB *iniciaTLB(int entradas_tlb) {

	TLB *tlb;
	int i;	
	
	qtd_tlb = 0;
	lru_aux_index = 0;

	if (entradas_tlb == 4 || entradas_tlb == 8) {
	
		tlb = malloc(entradas_tlb*sizeof(TLB));
	
		for (i = 0; i < entradas_tlb; i++) {
	
			tlb[i].residencia = 0;
			tlb[i].endereco = -1;
		}	
		return tlb;
	}
	return NULL;
}

struct stats *iniciaInfo() {
	
	struct stats *info = malloc(sizeof(struct stats));
	
	info->hits_l1 = 0; 
    	info->misses_l1 = 0;
    	info->hits_tlb = 0; 
    	info->misses_tlb = 0; 
    	info->cycles = 0;
	return info;
}

bool verificaTLB(TLB *tlb, int endereco_convertido, int entradas_tlb) {

	int i = 0;
	
	endereco_convertido = endereco_convertido>>10; //TLB usa 10 bits de offset. Como só importa saber se o endereço está ou não na TLB, a posição dele dentro do bloco não interessa

	if (qtd_tlb == 0) {
		
		lru = 0;
	}

	for (i = 0; i < entradas_tlb; i++) {	
		
		//verifica se vai dar hit	
		if ((tlb[i].residencia == true) && (tlb[i].endereco == endereco_convertido)) {

			lru_aux[lru_aux_index] = i;		
			lru_aux_index++;	
			
			if (lru == i) {

				lru_pos++;
				lru = lru_aux[lru_pos];
			}
			return true;
		}
	}
	
	//miss na TLB
	if (qtd_tlb < entradas_tlb) {

		tlb[qtd_tlb].endereco = endereco_convertido; 
		tlb[qtd_tlb].residencia = true;
		
		lru_aux[lru_aux_index] = qtd_tlb;
		lru_aux_index++;

		qtd_tlb++;
		//printf("\nqtd_tlb = %d\n", qtd_tlb);
		return false;
	}

	//se essa parte do algoritmo executar, significa que o endereço procurado deu miss e a TLB está cheia. Sendo assim, é necessário a substitução do bloco LRU.	
	lru = lruTLB();
	tlb[lru].endereco = endereco_convertido;
	
	lru_aux[lru_aux_index] = lru;		
	lru_aux_index++;
	lru_pos++;
	lru = lru_aux[lru_pos];

	return false;	
}

int lruTLB() {

	int i, j, test_lru = 0;

	for (i = lru_pos; i < lru_aux_index; i++) {
		for (j = lru_pos+1; j < lru_aux_index; j++) {

			if (lru == lru_aux[j]) {
				
				lru_pos++;
				lru = lru_aux[lru_pos];
				test_lru = 1;
				break;
			}
		}

		if (!test_lru) {
		
			return lru;
		}
	}
	return lru;		
}

bool verificaCache(CACHE *cache, int endereco_convertido, int tamanho_bloco, int tamanho_cache, char operacao) {

	int endereco_cache, tag;

	endereco_cache = (endereco_convertido/tamanho_bloco)%qtd_blocos;
	tag = endereco_convertido>>tam_tag;

	if (operacao == 'R') {	
	
		if (tamanho_cache == 8192) {

			//info->cycles++;
		
			//verifica se o bloco já foi carregado na cache
			if (cache[endereco_cache].residencia) {
			
				//verifica hit
				if (cache[endereco_cache].tag[0] == tag) {

					lru_cache = 1;
					return true;
				}
			
				//verifica hit		
				if (cache[endereco_cache].tag[1] == tag) {
					
					lru_cache = 0;
					return true;
				}
				
				//miss
				if (cache[endereco_cache].tag[1] == -1) {
					
					cache[endereco_cache].tag[1] = tag;
					return false;
				}
				
				//se não ocorrer nenhum dos três casos anteriores, o bloco da cache está cheio e o LRU deve ser substituído
				
				cache[endereco_cache].tag[lru_cache] = tag;
				lru_cache = !lru_cache;
				return false;				
				
			} else {

				cache[endereco_cache].tag[0] = tag;
				cache[endereco_cache].residencia = true;
				lru_cache = 0;
				return false;
			}
		}

		if (tamanho_cache == 16384) {
			
			//verifica se o bloco já foi carregado na cache
			if (cache[endereco_cache].residencia) {
		
			//verifica hit
				if (cache[endereco_cache].tag[0] == tag) {
			
					return true;
				}
			
				//miss
				cache[endereco_cache].tag[0] = tag;
				return false;
			} else {
			
				cache[endereco_cache].tag[0] = tag;
				cache[endereco_cache].residencia = true;
				return false;
			}
		}
	} else {
		
		if (cache[endereco_cache].residencia) {
				
			if ((cache[endereco_cache].tag[0] == tag) || (cache[endereco_cache].tag[1] == tag)) return true;
			else return false;
				
		} else return false;
	}
	return false;
}

				
/** Recebe um traço de endereços de memória acessados e simula hierarquia de memória
  * @param tamanho_bloco tamanho do bloco (32 ou 64 bytes)
  * @param tamanho_cache tamanho da cache de dados (8 ou 16 KiB)
  * @param entradas_tlb número de entradas na TLB (4 ou 8)
  * @param filename nome do arquivo de traço (ou null)
  * @param stream stream com traço (se filename for null)
  * @return estatísticas de simulação
  */
struct stats * sim(int tamanho_bloco, int tamanho_cache, int entradas_tlb, char * filename, char * stream) {

	struct stats *info;
	CACHE *cache;
	TLB *tlb;
	FILE *arq;
	char operacao;
	char endereco_inicial[9]; //endereco oriundo do arquivo de teste
	int endereco_convertido; //endereco transformado para o tipo "int"
	int j = 0, i;
	
	lru_aux = malloc(6000*sizeof(int));
	
	cache = iniciaCache(tamanho_bloco, tamanho_cache);
	tlb = iniciaTLB(entradas_tlb);
	info = iniciaInfo();


	if (!cache || !tlb) return NULL;

	if (filename) {

		arq = fopen(filename, "r");
		while (fscanf(arq, "%c %s\n", &operacao, endereco_inicial) != EOF) {
		
			endereco_convertido = strtol(endereco_inicial, NULL, 16);			
				
			if (verificaTLB(tlb, endereco_convertido, entradas_tlb)) {
			
				info->hits_tlb++;
				hits++;
				
				if (entradas_tlb == 4) info->cycles++;
				else info->cycles += 2;
			} else {
				
				if (entradas_tlb == 4) info->cycles++;
				else info->cycles += 2;	
				info->misses_tlb++;
				info->cycles += 50;
				miss++;
			}
			
			if (verificaCache(cache, endereco_convertido, tamanho_bloco, tamanho_cache, operacao)) {

				info->hits_l1++;
				if (operacao == 'W') {
	
					info->cycles += 50; //se for escrita, sempre vai acessar a memória principal
					hits_escrita++;
				} else {

					hits_leitura++;
				}
				hits++;
				if (tamanho_cache == 8192) info->cycles++;
				else info->cycles += 2;
			} else {
					
				if (tamanho_cache == 8192) info->cycles++;
				else info->cycles += 2;
				
				miss++;
				info->misses_l1++;
				info->cycles += 50;
			}
		}
		cache = NULL;		
		tlb = NULL;
		
		free(cache);
		free(tlb);
		fclose(arq);


		qtd_tlb = 0;
		lru_aux_index = 0;
		lru_pos = 0;
		lru = 0;
		qtd_blocos = 0;
		lru_cache = 0;
		tam_tag = 0;

		return info;

	} 
	
	if (stream) {

		while (stream[j] != '\0') {
			
			i = 0;

			operacao = stream[j];
			j += 2;
			for (; stream[j] != '\n'; j++) {
					
				endereco_inicial[i] = stream[j];
				i++;
			}
			j++; //incrementa pois não queremos o '\n'
			endereco_inicial[i] = '\0'; //final da string com o endereço
			endereco_convertido = strtol(endereco_inicial, NULL, 16);
		
			if (verificaTLB(tlb, endereco_convertido, entradas_tlb)) {

				info->hits_tlb++;
				
				if (entradas_tlb == 4) info->cycles++;
				else info->cycles += 2;
			} else {
				
				if (entradas_tlb == 4) info->cycles++;
				else info->cycles += 2;	
				info->misses_tlb++;
				info->cycles += 50;
			}

			if (verificaCache(cache, endereco_convertido, tamanho_bloco, tamanho_cache, operacao)) {

				info->hits_l1++;
				if (operacao == 'W') {
	
					info->cycles += 50; //se for escrita, sempre vai acessar a memória principal
					hits_escrita++;
				} else {

					hits_leitura++;
				}
				if (tamanho_cache == 8192) info->cycles++;
				else info->cycles += 2;
			} else {
				
				if (tamanho_cache == 8192) info->cycles++;
				else info->cycles += 2;
				info->misses_l1++;
				info->cycles += 50;
			}
		}
		
		cache = NULL;		
		tlb = NULL;
		free(cache);
		free(tlb);
		
		qtd_tlb = 0;
		lru_aux_index = 0;
		lru_pos = 0;
		lru = 0;
		qtd_blocos = 0;
		lru_cache = 0;
		tam_tag = 0;
		return info;
	}
	
	return NULL;		
}	
