# Relatório - João Pedro Kohls Bast

## 1) Funções e estruturas

Para facilitar a compreensão do trabalho, assim como sua organização, foi desenvolvido um total de seis funções auxiliares. As funções apresentam os seguintes _labels_:

- CACHE *iniciaCache(int tamanho_bloco, int tamanho_cache);

A função à cima tem como objetivo definir o tamanho e a quantidade de blocos que a cache terá, de acordo com a configuração desejada.

- TLB *iniciaTLB(int entradas_tlb);

Assim como a função _iniciaCache_, esta função é responsável por definir o formato da TLB de acordo com a configuração desejada.

- struct stats *iniciaInfo();

Esta função apenas inicializa os valores da estrutura do tipo _stats_ como zero.

- bool verificaTLB(TLB *tlb, int endereco_convertido, int entradas_tlb);

O objetivo desta função é verificar se um dado endereço está presente ou não na TLB. Para isso, o endereço requerido é passado por parâmetro e tem o seu valor deslocado dez vezes para a direita, pois não utilizamos os bits de _offset_ na hora de verificar se deu _hit_ ou _miss_. Depois disso, comparamos o valor do endereço deslocado com os atuais endereços que estão presente na TLB. Se estiver, a função retorna _true_. Caso contrário, retorna _false_.

- int lruTLB();

A função lruTLB() serve para identificar qual entrada da TLB deve ser substituída caso ela esteja cheia, dando lugar a uma nova entrada oriunda da tabela de páginas. Seu funcionamento se dá da seguinte maneira:
O vetor lru_aux armazena todas as posições em ordem de acesso da TLB (isso é feito dentro da função verificaTLB). Toda vez que a função lruTLB for acionada, dois laços são usados para percorrer este vetor. O primeiro, fixa um elemento do vetor e o segundo, percorre o restante do vetor para ver se a posição escolhida no primeiro laço foi acessada posteriormente. Se isso for verdade, o elemento fixado não será substituído. O laço é executado até que o elemento fixado não tenha sido acessado posteriormente, se tornando a posição a ser substituída.
Vale ressaltar que, o primeiro elemento a ser fixado no primeiro laço, não é escolhido de maneira aleatória. Ele é escolhido dentro da função verificaTLB com o objetivo de otimizar o processo feito na função lruTLB. Caso o elemento escolhido seja sempre o primeiro do vetor lru_aux, o processo se tornaria muito custoso se a TLB fosse acessada muitas vezes.

- bool verificaCache(CACHE *cache, int endereco_convertido, int tamanho_bloco, int tamanho_cache, char operacao);
	
Funciona do mesmo modo que a _verificaTLB_, retorna _true_ se der _hit_ e _false_ se der _miss_. A função apenas difere no tratamento de escrita, que mesmo se der _miss_ não substitui e não altera nenhum dado da cache, apenas na memória principal por conta do _write through_. A política do LRU é sempre utilizada na cache 2-associativa substituindo o penúltimo endereço acessado dentro de um bloco. Se for mapeamento direto, a sbustituição ocorre sempre que houver um _miss_.

Sobre as estruturas usadas.
Foi utilizado uma estrutura do tipo CACHE para armazenar a tag dos endereços e o bit de residência. Se esse bit estiver em zero (false), quer dizer que aquela bloco da cache está vazio, gerando um miss compulsório.
A outra estrutura, do tipo TLB, serve para armazenar o endereço que está presente nas entradas da TLB, e o bit de residência, que funciona do mesmo modo que na cache.

## 2) Função principal (sim())

Basicamente, esta função faz a leitura dos endereços a partir de um arquivo, se _filename_ for diferente de _NULL_, convertendo-os de _string_ para _int_ para que possam ser utilizados nos cálculos posteriores. Ela também é responsável por chamar as funções descritas anteriormente fazendo os cálculos de _cycles_, _hits_ e _misses_ de acordo com o retorno destas funções.

## 3) Diretório aluno

Na pasta aluno, está contido o arquivo de teste _aluno.c_ e também, os arquivos de texto no formato MAC e mais alguns casos de teste criados por mim para testar, principalmente, o funcionamento da função _lruTLB()_.







	

